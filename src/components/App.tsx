import React, { ReactElement } from 'react';
import { BrowserRouter } from 'react-router-dom';
import '../App.css';
import StoreProvider from '../shared/StoreProvider';
import Layout from './Layout';
import Routes from './Routes';


function App():ReactElement  {
  return (
    <StoreProvider>
      <BrowserRouter>
        <Layout>
          <Routes />
        </Layout>
      </BrowserRouter>
    </StoreProvider>
  );
}
export default App;
