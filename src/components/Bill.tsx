import React, { ReactElement } from 'react'
import { useStore } from '../shared/StoreProvider';
import COCKTAIL from '../types/Cocktail';

export default function Bill():ReactElement {
  const {store,dispatch}=useStore()
  const {drinksArray} = store;

  const countItems=(drink:COCKTAIL):number=>{
    return drinksArray.filter(drink_ => drink_.idDrink === drink.idDrink).length
  }

  const getUnique= drinksArray.reduce((acc:COCKTAIL[],drink)=>{
    acc.find(drink_=>drink_.idDrink === drink.idDrink) || acc.push(drink)
    return acc
  },[]).sort((drinkA,drinkB)=> Number(drinkA.idDrink)-Number(drinkB.idDrink));

  const total =()=>{
    return getUnique.reduce(function(net:number,drink){ return (net+(countItems(drink)*10))},0)
  }

  // console.log(getUnique)
  if (getUnique.length===0) {return <div className="card"> <div className="content">
    <div className="ui brown header">Cart is empty</div></div></div>}

    
  return (
    <div className="ui grid container">
      <div className="four wide column">
        {
          getUnique.map((drink)=>
            <div key={drink.idDrink}>
              <div className="ui three column grid">
                <div className="column"><h5 className="ui  orange  header">{drink.strDrink}</h5></div>
                <div className="column"><a className="ui basic green label " style={{marginLeft:100}}>{countItems(drink)}</a></div>
                <div className="column" ><span className="ui basic purple label " style={{marginLeft:100}}>{countItems(drink)*10}</span></div>
              </div>    
              <div className="ui  divider"> </div>          
            </div>
          )
        }
        <div className="ui horizontal divider">Total Amount </div>
        <h5 className="right floated "> Amount  to be paid  {total()}  €</h5>
        <div className="ui horizontal divider">--------------------------------------------------- </div>
      </div>   
      
    </div>
  )
}
