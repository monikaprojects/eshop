import React, { ReactElement } from 'react'
import {  useStore } from '../shared/StoreProvider'
import COCKTAIL from '../types/Cocktail';
import CocktailListItem from './CocktailListItem';

 
export default function Cart():ReactElement{
  const {store,dispatch}=useStore()
  const {drinksArray} = store;

  const addToCart =(e:React.SyntheticEvent,drink:COCKTAIL):void=>{
    e.stopPropagation();
    dispatch({type:'AddToCart',drink})
  }

  const removeFromCart =(e:React.SyntheticEvent,drink:COCKTAIL):void=>{
    e.stopPropagation();
    dispatch({type:'DeleteFromCart',drink})
  }

  const countItems=(drink:COCKTAIL)=>{
    return drinksArray.filter(drink_ => drink_.idDrink === drink.idDrink).length
  }

  const getUnique= drinksArray.reduce((acc:COCKTAIL[],drink)=>{
    acc.find(drink_=>drink_.idDrink === drink.idDrink) || acc.push(drink)
    return acc
  },[]).sort((drinkA,drinkB)=> Number(drinkA.idDrink)-Number(drinkB.idDrink));

  if (getUnique.length===0) {return <div className="card"> <div className="content">
    <div className="ui brown header">Cart is empty</div></div></div>}

  return (
    <>
      {
        getUnique.map((drink)=><>
          <CocktailListItem key={drink.idDrink} drink={drink}>
            <div className="right floated content"> 
              <div className="ui labeled button" >
                <div className="ui yellow button" onClick={(e)=>{addToCart(e,drink)}}>
                  <i className="shopping cart icon"></i> Add
                </div>
                <div className="ui red button" onClick={(e)=>{removeFromCart(e,drink)}}>-

                </div>
                <a className="ui basic label">
                  {countItems(drink)}
                </a>
              </div>
              
            </div>
          </CocktailListItem>
          <div className="ui horizontal divider">-</div>
        </>)
      } 
    </>
  )
}