import React, { ReactElement } from 'react'
import { useCocktailApi } from '../shared/cocktailApi'
import CATEGORY from '../types/category'
import CategoryListItem from './CategoryListItem'


export default function CategoriesList() :ReactElement{
  const [cList,setCList] = useCocktailApi<{drinks:CATEGORY[]}>('/api/json/v1/1/list.php?c=list')

  if (!cList) {return <div className="ui active inverted dimmer"><div className="ui text loader large">Loading ...</div></div>}

  const categoryList = cList.drinks

  // console.log(encodeURIComponent("https://www.thecocktaildb.com/api/json/v1/1/list.php?c=Coffee/Tea"))
  return (
    <div className="ui cards" style={{padding: 20}}>
      {
        categoryList.map((category)=><CategoryListItem key={category.strCategory} category={category}></CategoryListItem>)     
      }      
    </div>
  )
}
