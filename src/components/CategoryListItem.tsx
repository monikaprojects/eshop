import React, { ReactElement } from 'react'
import { useHistory } from 'react-router-dom'
import CATEGORY from '../types/category'


interface Props{
    category:CATEGORY
}
export default function CategoryListItem(props:Props) :ReactElement{
  const history=useHistory();
  const arrInit:String[]=['img1.jpg','img2.jpg','img3.jpg','img4.jpg','img5.jpg','img6.jpg','img7.jpg','img8.jpg','img9.jpg','img10.jpg','img11.jpg','img12.jpg','img13.jpg','img14.jpg',
    'img15.jpg','img16.jpg','img17.jpg','img18.jpg','img19.jpg','img20.jpg','img21.jpg','img22.jpg','img23.jpg','img24.jpg','img25.jpg','img26.jpg','img27.jpg','img28.jpg','img29.jpg','img30.jpg']

  const goToList=(e:React.SyntheticEvent)=>{
    e.stopPropagation();
    console.log(e.currentTarget.textContent)
    const str = e.currentTarget.textContent ||'';

    // history.push(`/categories/${str.trim().replace(' ','_')}`)
    history.push(`/categories/${encodeURIComponent(str.replaceAll(" / ", "/").trim().replace(' ','_'))}`)
  }

  const indexSrc=Math.ceil(Math.random()*14);
  // console.log(indexSrc)

  return (
    <div  className="ui red card" onClick={goToList}>
      <img  className="ui tiny bordered rounded" src={`../../images/`+arrInit[indexSrc]} width={100} height={100}></img>
      <div className="item right floated" style={{margin: 15}}>
        <h3 className="ui violet header"> { props.category.strCategory}</h3>
      </div>
    </div>
  )
}
