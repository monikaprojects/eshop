import React, { ReactElement } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { useCocktailApi } from '../shared/cocktailApi';
import {  useStore } from '../shared/StoreProvider';
import COCKTAIL from '../types/Cocktail';



export default function CocktailDetails() :ReactElement{
  const {id}= useParams<{id:string}>();
  const history=useHistory();
  const [drinksArray,setDrinksArray] = useCocktailApi<{drinks:COCKTAIL[]}>(`/api/json/v1/1/lookup.php?i=${id}`)
  const {store,dispatch}=useStore()

  

  console.log(`/api/json/v1/1/lookup.php?i=${id}`)
  if(!drinksArray)  {return <p>loading</p>}

  const drink =drinksArray.drinks[0]
  

  const goToList =()=>{
    history.push(`/categories/${drink.strCategory.replace(' ','_')}`)
  }
  const addToCart =()=>{
    console.log("addToCart" ,drink);
    dispatch({type:"AddToCart",drink})
  }

  return (
    <div>
      <div>
        <h1>{drink.strDrink}</h1>
        <img src={drink.strDrinkThumb} height={100} width ={100} ></img>
        <div className="ui divider"></div>
        <div className="ui grid">
          <div className="four wide column">
            <h3>Ingredients</h3>
            {drink.strIngredient1 ? drink.strIngredient1 : '    ' + drink.strMeasure1}
            {drink.strIngredient2 ? ',' + drink.strIngredient2 : '    '  && drink.strMeasure2} 
            {drink.strIngredient3 ?  ',' +drink.strIngredient3 : '    ' && drink.strMeasure3} 
            {drink.strIngredient4 ?  ',' +drink.strIngredient4 : '    ' &&  drink.strMeasure4} 
            {drink.strIngredient5 ? ',' + drink.strIngredient5 : '    ' &&  drink.strMeasure5}  
            {drink.strIngredient6 ? ',' + drink.strIngredient6 : '    ' &&  drink.strMeasure6}  
            {drink.strIngredient7 ? ',' + drink.strIngredient7 : '    ' &&  drink.strMeasure7} 
            {drink.strIngredient8 ?  ',' +drink.strIngredient8 : '    ' &&  drink.strMeasure8}  
          </div>
          <div className="four wide column">
            <h3>Instructions</h3>
            {drink.strInstructions}
          </div>
        </div>
        <div className="ui divider"></div>
        <button onClick={goToList } className ="ui button yellow">Back</button>
        <button onClick={addToCart} className="ui green button">Add to Cart</button>
      </div>
    </div>
  )
}
