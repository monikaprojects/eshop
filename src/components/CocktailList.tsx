import React, { ReactElement } from 'react'
import { useParams } from 'react-router-dom';
import { useCocktailApi } from '../shared/cocktailApi';
import { useStore } from '../shared/StoreProvider';
import COCKTAIL from '../types/Cocktail';
import CocktailListItem from './CocktailListItem';


interface Props{
  pathParameter:string
}
export default function CocktailList(props:Props):ReactElement {
  const {id}= useParams<{id:string}>();
  const [drinksArray] = useCocktailApi<{drinks:COCKTAIL[]}>(`${props.pathParameter}`+`${(id==undefined)?'':decodeURIComponent(id)}`);   
  const {store,dispatch}=useStore()
  
  console.log(decodeURIComponent(id))
  console.log(`${props.pathParameter}`+`${(id==undefined)?'':id}`)
  if(!drinksArray) {return <p>No drinks available</p>}
  console.log(drinksArray)
  
  const addToCart=(e:React.SyntheticEvent,drink:COCKTAIL):void=>{
    e.stopPropagation();
    dispatch({type:"AddToCart",drink}) 
  }


  return (
    <div className="ui cards">
      {
        drinksArray.drinks.map((drink)=> <CocktailListItem key={drink.idDrink} drink={drink} >
          <button><i className="shopping basket icon" onClick={(e)=>addToCart(e,drink)}></i></button>
        </CocktailListItem>
            
        )
      }
    </div>
  )
}
