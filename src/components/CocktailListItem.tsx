import React, {  ReactElement } from 'react'
import { useHistory } from 'react-router-dom';
import { useStore } from '../shared/StoreProvider';
import COCKTAIL from '../types/Cocktail';


interface Props{
  drink : COCKTAIL
  children?:ReactElement
}

export default function CocktailListItem(props:Props):ReactElement {
  const {drink}=props;
  const history = useHistory();
  const {store,dispatch}=useStore()

  // console.log(drink)
  const gotoRecipe = (e:React.MouseEvent<HTMLDivElement>)=>{
    e.stopPropagation();
    // console.log(`cocktails/${drink.idDrink}`)
    history.push(`/cocktails/${drink.idDrink}`)
  }

  const likeThisItem=(e:React.SyntheticEvent):void=>{
    e.stopPropagation();
    
    if(store.wishList.find(current=>current.idDrink===drink.idDrink))
    {dispatch({type:'DeleteFromWishList',drink})}
    else
    {dispatch({type:'AddToWishList',drink})}
  }

  const favourThisItem=(e:React.SyntheticEvent):void=>{
    e.stopPropagation()
  }
  
  // console.log(`right floated like icon ${store.wishList.find(current=>current.idDrink===drink.idDrink)?'red':'grey'}`)

  return (
    <div className="card " onClick={(e)=>gotoRecipe(e)}>   
      <div className="content">
        <i className={`right floated like icon ${store.wishList.find(current=>current.idDrink===drink.idDrink)?'red':'grey'}`}  onClick={(e)=>likeThisItem(e)} ></i>
        <i className="right floated star icon" onClick={(e)=>favourThisItem(e)}></i>
        <img  className="ui tiny image" alt="" src={drink.strDrinkThumb?drink.strDrinkThumb:""} />
        <div className="content">
          <div className="ui brown header">
            {drink.strDrink}
          </div>
        </div>
      </div>
      <div className="right floated buttons">{props.children}</div> 
      
    </div>
  )
}
