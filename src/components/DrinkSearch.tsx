import React, { ChangeEvent, ReactElement, useState } from 'react'
import { useHistory } from 'react-router-dom'
import cocktailApi from '../shared/cocktailApi'
import COCKTAIL from '../types/Cocktail'

interface Props{
    className?:string
    headline?:string
}
export default function DrinkSearch(props:Props) :ReactElement{
  const [searchTerm, setSearchTerm] = useState('')
  const [searchResults, setSearchResults] = useState<{drinks:COCKTAIL[]}>({drinks:[]})
  const {drinks}=searchResults
  const history = useHistory()
    
  const onSearch = (e: ChangeEvent<HTMLInputElement>) => {
    const inputValue = e.target.value
    setSearchTerm(inputValue)
  
    console.log( `/api/json/v1/1/search.php?s="${inputValue}"`)
    if (inputValue.length > 2) {
      cocktailApi('get', `/api/json/v1/1/search.php?s=${inputValue}`, setSearchResults)
      console.log("length",searchResults)
    } else {
      setSearchResults({drinks:[]})
    }
  }

  const onClick = (drink: COCKTAIL) => {
    setSearchResults({drinks:[]})
    setSearchTerm('')
    history.push(`/cocktails/${drink.idDrink}`)
  }
  
  return (
    <>
      {props.headline && <h2>{props.headline}</h2>}
      <div className={`ui search ${props.className}`}>
        <div className="ui icon input">
          <input value={searchTerm} onChange={onSearch} type="text" className="prompt" />
          <i className="search icon" />
        </div>
        {drinks.length > 0 &&
          <div className="results transition visible">
            {drinks.map(drink =>
              <span onClick={() => onClick(drink)} key={drink.idDrink} className="result">
                {drink.strDrink}
              </span>
            )}
          </div>
        }
      </div>
    </>
  )
}
