import React, { ReactElement, ReactEventHandler } from 'react'
import { useHistory } from 'react-router-dom'
import DrinkSearch from './DrinkSearch';


export default function Home() :ReactElement{
  const history=useHistory();

  const gotoSignUpForm=()=>{
    history.push('/signUp')
  }
  const gotoSignInForm=()=>{
    history.push('/signIn')
  }
  
  return (
    <div>
      <div className="ui three column grid">
        <div className="column"><h2>Willkommen @ Hemmingway&apos;s </h2> </div>
        <div className="column right floated"><DrinkSearch headline="Search Drink"></DrinkSearch></div>
      </div>
      <img src='../../images/image-restaurant.jpg' width={700} height={500}></img>
      <br/>
      <br/>
      <button className="ui purple button" onClick={gotoSignUpForm}>Sign Up</button>
      <button className="ui yellow button" onClick={gotoSignInForm}>Sign In</button>
    </div>
  )
}
