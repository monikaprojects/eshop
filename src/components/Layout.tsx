import React, { ReactElement } from 'react'
import {  NavLink, useHistory } from 'react-router-dom'
import { useStore } from '../shared/StoreProvider'

interface Props{
  children:ReactElement
}
export default function Layout(props:Props) :ReactElement{
  const {store,dispatch}=useStore()
  const history=useHistory()

  const signOut=()=>{
    dispatch({type:'ChangeUser',user:'Guest'})
    history.push('/home')
  }

  return (
    <>
      <div className="ui compact olive six item inverted menu">
        <NavLink exact to='/home' className="active item " activeClassName="active "><h3>Home  <i className="home icon"></i></h3></NavLink>
        <NavLink exact to='/categories' className="item" activeClassName="active"><h3>Cocktail Menu  <i className="angle double right icon"></i></h3></NavLink>
        <NavLink exact to='/cart' className="item" activeClassName="active"><h3>Cart  <i className="cart icon"></i></h3></NavLink>
        <NavLink exact to='/wishlist' className="item" activeClassName="active"><h3>Wishlist  <i className="like icon"></i></h3></NavLink>
        <NavLink exact to='/bill' className="item" activeClassName="active"><h3>Bill  <i className="calculator icon"></i></h3></NavLink>
        <label  className="right floated content"><h3>{store.user}  <i className="user circle icon"></i><i className="sign out alternate icon" onClick={signOut}></i></h3></label>
      </div>
      <div className="ui container" style={{marginTop:50}}>
        {props.children}
        
      </div>
    </>
  )
}



