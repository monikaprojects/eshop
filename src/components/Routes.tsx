import React, { ReactElement } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import Bill from './Bill'
import Cart from './Cart'
import CategoriesList from './CategoriesList'
import CocktailDetails from './CocktailDetails'
import CocktailList from './CocktailList'
import Home from './Home'
import SignInForm from './SignInForm'
import SignUpForm from './SignUpForm'
import Wishlist from './Wishlist'

export default function Routes() :ReactElement{

  return (
    <Switch>
      <Route  path='/cocktails/:id'>
        <CocktailDetails  />
      </Route>
      <Route exact path='/categories/:id'>
        <CocktailList pathParameter={`/api/json/v1/1/filter.php?c=`} />
      </Route>
      <Route exact path='/categories'>
        <CategoriesList />
      </Route>
      <Route exact path='/cart'>
        <Cart />
      </Route>
      <Route exact path='/wishlist'>
        <Wishlist></Wishlist>
      </Route>
      <Route exact path='/bill'>
        <Bill></Bill>
      </Route>
      <Route exact path='/signUp'>
        <SignUpForm></SignUpForm>
      </Route>
      <Route exact path='/signIn'>
        <SignInForm></SignInForm>
      </Route>
      <Route exact path='/home'>
        <Home />
      </Route>
      <Route exact path='/'>
        <Redirect to='/home'></Redirect>
      </Route>
    </Switch>
  )
}
