import React, { FormEventHandler, ReactElement, useState } from 'react'
import { useHistory } from 'react-router-dom';
import { useStore } from '../shared/StoreProvider';
import { useUserApi } from '../shared/userApi';
import USER from '../types/user';
import css from './SignInForm.module.css'

export default function SignInForm():ReactElement {
  const [email, setEmail] = useState<string>('')
  const [passwort, setPasswort] = useState<string>('')
  const [users]= useUserApi<USER[]>('/users')
  const history= useHistory();
  const {dispatch}=useStore()
  

  if(!users) {return <p>Guest Login </p>}

  const checkUserExist=():boolean=>{
    return users.find((user)=>user.email === email) ? true :false
  }

  const goHome=()=>{
    history.push("/home");
  }
  

  const onSubmit=(e:React.FormEvent<HTMLFormElement>)=>{
    e.preventDefault();
    if(checkUserExist()){
      dispatch({type:'ChangeUser',user:email})
      history.push('/categories')
    }
  }

  return (
    <form className={`ui form ${css.signInForm}`} onSubmit={onSubmit}>
      <label> Email Address</label>
      <input value={email} onChange={(e)=>setEmail(e.target.value)} required type='email' />

      <label>Passwort</label>
      <input value={passwort} type="password" onChange={(e)=>setPasswort(e.target.value)} required minLength={6}/>     
      <div> 
        <button className="ui yellow button" onClick={goHome}>Back</button>
        <button className="ui green button">Sign In</button>
      </div>
    </form>
  )
}
