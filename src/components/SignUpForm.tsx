import React, {  ReactElement, useState } from 'react'
import { useHistory } from 'react-router-dom'
import userApi from '../shared/userApi'
import  css from './SignUpForm.module.css'

export default function SignUpForm():ReactElement {
  const [name, setName] = useState<string>('')
  const [email, setEmail] = useState<string>('')
  const [passwort, setPasswort] = useState<string>('')
  const history= useHistory();

  const onSubmit=(e:React.FormEvent<HTMLFormElement>)=>{
    e.preventDefault();
    userApi('post','/users',()=>{history.push('/signIn')},{name,email,passwort})

  }
  
  const goHome=()=>{
    history.push("/home");
  }
  
  return (
    <form className={`ui form ${css.signUpForm}`} onSubmit={onSubmit}>
      <label>Name</label>
      <input  value={name} onChange={(e)=>setName(e.target.value)} required minLength={2}/>

      <label>Passwort</label>
      <input value={passwort} type="password" onChange={(e)=>setPasswort(e.target.value)} required minLength={6}/>

      <label> Email Address</label>
      <input value={email} onChange={(e)=>setEmail(e.target.value)} required type='email'/>
      <div>
        <button className="ui yellow button" onClick={goHome}>Back</button>
        <button className="ui green primary button" >Sign Up</button>
      </div>
    </form>
  )
}
