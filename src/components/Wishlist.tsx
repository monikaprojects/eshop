import React, { ReactElement } from 'react'
import { useStore } from '../shared/StoreProvider';
import CocktailListItem from './CocktailListItem';

export default function Wishlist() :ReactElement{
  const {store,dispatch}=useStore()
  const {wishList} = store;

  if (wishList.length===0) {return <div className="card"> <div className="content">
    <div className="ui brown header">Wishlist is empty</div></div></div>}

  return (
    <>
      {
        wishList.map((drink)=><>
          <CocktailListItem key={drink.idDrink}  drink={drink}></CocktailListItem>
          <div className="ui horizontal divider">-</div>
        </>)
      } 
    </>
  )
}
