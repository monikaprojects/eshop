import React, { createContext, Dispatch, ReactElement, useContext, useReducer } from "react";
import COCKTAIL from "../types/Cocktail";

export interface ICart{
    user : string,
    drinksArray : COCKTAIL[],
    wishList: COCKTAIL[]
}

export const initState :ICart={
  user:'Guest',
  drinksArray :[],
  wishList: []
}

interface IAddToCart{
    type : "AddToCart",
    drink : COCKTAIL
}
interface IDeleteFromCart{
    type : "DeleteFromCart",
    drink : COCKTAIL
}

interface IChangeUser{
  type:"ChangeUser",
  user :string
}

interface IAddToWishList{
  type:"AddToWishList"
  drink:COCKTAIL
}

interface IDeleteFromWishList{
  type:"DeleteFromWishList"
  drink:COCKTAIL
}


type Actions = IAddToCart|IDeleteFromCart|IChangeUser|IAddToWishList|IDeleteFromWishList;
export type DispatchActions = React.Dispatch<Actions>;

export const reducer= (cart:ICart,action:Actions):ICart=>{
  switch (action.type){
  case "AddToCart" : return {...cart,drinksArray : [...cart.drinksArray,action.drink]};
  case "DeleteFromCart":{
    const index = cart.drinksArray.map(drink => drink.idDrink).indexOf(action.drink.idDrink)
    return {...cart, drinksArray: cart.drinksArray.filter((_drink, i) => i !== index)}
  }
  case "ChangeUser": return{...cart,user:action.user}
  case "AddToWishList" : return {...cart,wishList : [...cart.wishList,action.drink]};
  case "DeleteFromWishList":{
    const index = cart.wishList.map(drink => drink.idDrink).indexOf(action.drink.idDrink)
    console.log(index)
    return {...cart, wishList: cart.wishList.filter((_drink, i) => i !== index)}
  }
  default : return initState
  }
}
//-----------------------------
interface StoreContext {
  store: ICart;
  dispatch: Dispatch<Actions>;
}

const StoreContextP = createContext({} as StoreContext);

export const useStore = (): StoreContext => useContext(StoreContextP);

export default function StoreProvider(props: {children: ReactElement, store?: ICart}): ReactElement {
  const [store, dispatch] = useReducer(reducer, props.store || {drinksArray: [],user:'Guest',wishList:[]});

  return (
    <StoreContextP.Provider value={{store, dispatch}}>
      {props.children}
    </StoreContextP.Provider>
  );
}
