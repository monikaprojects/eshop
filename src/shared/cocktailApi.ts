
import axios, { AxiosResponse, Method } from "axios";
import { useEffect, useState } from "react";

type Setter<T> = (data: T) => void

export default function cocktailApi<T>(method:Method,path:string,callback:(data:T)=>void,data={}):void
{
  const baseUrl = "https://thingproxy.freeboard.io/fetch/https://www.thecocktaildb.com";  
  // const baseUrl = "https://cors-anywhere.herokuapp.com/https://www.thecocktaildb.com"; 
  // const baseUrl = "https://yacdn.org/proxy/https://www.thecocktaildb.com";
  // const baseUrl = "http://alloworigin.com/get?https://www.thecocktaildb.com";
  // c// onst baseUrl = "https://archive.org/www.thecocktaildb.com";

  console.log(baseUrl+path)
  axios({
    method,
    url: baseUrl+path,
    data,
    headers: {'x-rapidapi-host': 'the-cocktail-db.p.rapidapi.com'}
  }).then((response: AxiosResponse<T>) => {
    callback(response.data)
    // console.log(JSON.stringify(response.data))
  })
  
}

export function useCocktailApi<T>(path:string):[T|undefined,Setter<T>]{
  const [data, setData] = useState<T>();

  useEffect(() => {
    cocktailApi<T>('get',path,setData);
  }, [path])
  
  return [data,setData]
}
