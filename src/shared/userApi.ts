
import axios, { AxiosResponse, Method } from "axios";
import { useEffect, useState } from "react";

type Setter<T> = (data: T) => void

export default function userApi<T>(method:Method,path:string,callback:(data:T)=>void,data={}):void
{
  const baseUrl = "http://localhost:3001";  

  axios({
    method,
    url: baseUrl+path,
    data,
  }).then((response: AxiosResponse<T>) => {
    callback(response.data)
    console.log(JSON.stringify(response.data))
  })
  
}

export function useUserApi<T>(path:string):[T|undefined,Setter<T>]{
  const [data, setData] = useState<T>();

  useEffect(() => {
    userApi<T>('get',path,setData);
  }, [path])
  
  return [data,setData]
}